#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Author: Per Unneberg
Created: Wed Feb  8 09:26:42 2017

"""
import os
from os.path import join as pjoin, abspath, normpath, dirname
import logging
import pytest
import yaml
import subprocess as sp
import shlex
import time
from lts_workflows.pytest import plugin as lts_pytest

try:
    import docker
except:
    pass

try:
    from pytest_ngsfixtures import config as fixture_config
    from pytest_ngsfixtures import layout as sample_layout
    from pytest_ngsfixtures.file import FixtureFile
    from pytest_ngsfixtures.os import safe_mktemp
    has_ngsfixtures = True
except:
    has_ngsfixtures = False

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ROOTDIR = abspath(normpath(pjoin(dirname(__file__), os.pardir)))
TESTDIR = pjoin(ROOTDIR, "tests")
EXAMPLESDIR = pjoin(TESTDIR, "examples")
MISSING_PYTEST_NGSFIXTURES = """
Test layouts empty; missing pytest-ngsfixtures

Install with 'conda install -c percyfal pytest-ngsfixtures'
"""
LAYOUT = 'individual'
RUNFMT = "{SM}/{SM}_{PU}"

TIMEOUT = 10
# Docker setup
DOCKER_STACK_NAME = "lts_scrnaseq"
DOCKER_REPO = os.environ.get("DOCKER_REPO", "scilifelablts")
WORKFLOW_IMAGE = DOCKER_REPO + "/lts-workflows-sm-scrnaseq:latest"
WORKFLOW_CONTAINER = "snakemake"
WORKFLOW_SERVICE = DOCKER_STACK_NAME + "_snakemake"
DOCKER_COMPOSE_NETWORK_NAME = DOCKER_STACK_NAME + "_default"

ENVIRONMENT_FILES = {
    'py3': lts_pytest.environment_files(path=ROOTDIR, testdir=TESTDIR,
                                        filters=("environment.yaml",)),
    'py2': lts_pytest.environment_files(path=ROOTDIR, testdir=TESTDIR,
                                        filters=("environment-27.yaml",))}


def pytest_configure(config):
    config.addinivalue_line("markers",
                            "missing_pytest_ngsfixtures: mark test as dependent on pytest_ngsfixtures")
    if has_ngsfixtures:
        config.option.ngs_layout = list(set(config.option.ngs_layout))
    else:
        config.option.ngs_layout = []
    config.option.sequencing_modes = list(set(config.option.sequencing_modes))
    config.option.ngs_command = list(set(config.option.ngs_command))
    config.option.ngs_unit = list(set(config.option.ngs_unit))


def pytest_namespace():
    d = lts_pytest.namespace(**ENVIRONMENT_FILES)
    d.update({'examplesdir': EXAMPLESDIR,
              'snakefile': pjoin(ROOTDIR, "tests", "examples",
                                 "Snakefile"),
              'configfile': pjoin(ROOTDIR, "tests", "examples",
                                  "config.yaml"),
              'has_ngsfixtures': has_ngsfixtures,
              'testdir': TESTDIR})
    return d


def pytest_addoption(parser):
    group = parser.getgroup("lts_workflows_sm_scrnaseq",
                            "single cell rna sequencing options")
    lts_pytest.addoption(group)
    units = ["local", "docker", "conda"]
    group.addoption(
        "--ngs-test-unit", action="store",
        choices=units,
        default=["local"],
        dest="ngs_unit",
        nargs="+",
        metavar="unit",
        help="test unit - test source code locally with or without use-conda or distributed in docker container (choices: {})".format(",".join(units))
    )
    group.addoption(
        "--smode", "--sequencing-modes", action="store",
        help="sequencing modes (single end) to run",
        nargs="+",
        dest="sequencing_modes", default=["se"], choices=["se"])
    run = ["run", "list", "runall"]
    group.addoption(
        "--ngs-test-command", action="store",
        choices=run,
        default=["run", "list"],
        dest="ngs_command",
        nargs="+",
        metavar="command",
        help="test command - run or list"
    )


def pytest_report_header(config):
    return lts_pytest.report_header(config)


def pytest_runtest_setup(item):
    lts_pytest.slow_runtest_setup(item)
    missing_pytest_ngsfixtures = item.get_marker("missing_pytest_ngsfixtures")
    if missing_pytest_ngsfixtures is not None and not has_ngsfixtures:
        pytest.skip(MISSING_PYTEST_NGSFIXTURES)


def stack_deploy(name, docker_compose):
    logger.info("Deploying stack {}".format(name))
    res = sp.check_output(shlex.split('docker stack ls'))
    stacks = [s.split(" ")[0] for s in res.decode().split("\n")]
    if name in stacks:
        logger.info("Stack {} already deployed; updating".format(name))
    else:
        # Implicitly assumes docker swarm init has been run
        sp.check_output(
            shlex.split('docker stack deploy --with-registry-auth -c {} {}'.format(docker_compose, name)))
        # Need to wait for containers to come up
        logger.info("Sleeping {} seconds to let containers launch".format(TIMEOUT))
        time.sleep(TIMEOUT)


def stack_rm():
    logger.info("Finalizing session")
    res = sp.check_output(shlex.split('docker stack ls'))
    stacks = [s.split(" ")[0] for s in res.decode().split("\n")]
    if DOCKER_STACK_NAME in stacks:
        logger.info("Removing docker stack {}".format(DOCKER_STACK_NAME))
        sp.check_output(
            shlex.split('docker stack rm {}'.format(DOCKER_STACK_NAME)))
        logger.info("Pruning docker volumes")
        sp.check_output(
            shlex.split('docker volume prune -f'))


def get_image(tag):
    client = docker.from_env()
    try:
        img = client.images.get(tag)
    except:
        raise
    return img


def get_container(service):
    """Get container corresponding to a service"""
    logger.info("Getting container information for service {}".format(service))
    client = docker.from_env()
    try:
        srv = client.services.get(service)
    except:
        raise
    try:
        cname = "{}.1.{}".format(service, srv.tasks()[0]['ID'])
        container = client.containers.get(cname)
    except:
        raise
    return container


# Session-scoped fixtures
@pytest.fixture(scope="session")
def docker_compose(request):
    return pjoin(pytest.testdir, "docker-compose.yaml")


@pytest.fixture(scope="session")
def cluster(request, docker_compose):
    """Setup containers and stack if requested"""
    def is_local(request):
        units = request.config.option.ngs_unit
        return set(units).issubset({"local", "conda"})

    if is_local(request):
        return
    request.addfinalizer(stack_rm)
    try:
        # Implicitly assumes docker swarm init has been run
        stack_deploy(DOCKER_STACK_NAME, str(docker_compose))
    except:
        logger.warn("Failed to setup slurm/workflow stack")
        raise


@pytest.fixture(scope="session")
def reference_data(request, tmpdir_factory):
    path = safe_mktemp(tmpdir_factory, "ref")
    copy = request.config.option.ngs_copy
    if set(request.config.option.ngs_unit).intersection(["docker"]):
        copy = True
    path = sample_layout.setup_reference_layout(
        path,
        label="ref", copy=copy,
        ignore_errors=True,
        scope="function")


def setup_workflow_data(request, tmpdir_factory, node_id):
    command, mode, unit = request.param
    datapath = node_id
    kwargs = {
        'layout': LAYOUT,
        'copy': request.config.option.ngs_copy,
        'runfmt': RUNFMT,
        'sampleinfo': True,
    }
    if unit in ["docker"]:
        kwargs['copy'] = True
    if command == "list":
        # Setup sampleinfo only
        path = safe_mktemp(tmpdir_factory, datapath)
        path = sample_layout.setup_sample_layout(path, setup=False,
                                                 scope="function", **kwargs)
        return path
    # Setup data
    path = safe_mktemp(tmpdir_factory, datapath)
    path = sample_layout.setup_sample_layout(path, scope="function", **kwargs)
    return path


def setup_metadata(request, tmpdir_factory, container, node_id):
    command, mode, unit = request.param
    datadir = node_id
    copy = request.config.option.ngs_copy
    if unit in ["docker"]:
        copy = True
    path = safe_mktemp(tmpdir_factory, dirname=datadir)
    kwargs = {'copy': copy, 'setup': True}
    FixtureFile(path.join("Snakefile"), src=pytest.snakefile, **kwargs)
    FixtureFile(path.join("config.yaml"), src=pytest.configfile, **kwargs)
    samples = {
        'individual': {'list': [], 'run': ['CHS.HG00512'],
                       'runall': ['CHS.HG00512', 'PUR.HG00731']}
    }
    with path.join("config.fmt.yaml").open("w") as fh:
        fh.write(yaml.dump(
            {'settings':
             {
                 'runfmt': '{SM}/{SM}_{PU}',
                 'samplefmt': '{SM}/{SM}'
             },
             'samples': samples[LAYOUT][command],
             'ngs.settings':
             {'is_pe': True if mode == "pe" else False}
            },
            default_flow_style=False))


def setup_container(unit):
    """Connect to the workflow or scheduler container"""
    if unit == "docker":
        SERVICE = WORKFLOW_SERVICE
    else:
        return None
    try:
        container = get_container(SERVICE)
        return container
    except:
        logger.warn("Failed to get container configuration for service {}".format(SERVICE))
        return None


@pytest.fixture(scope="function")
def runenv(request, tmpdir_factory, cluster, reference_data):
    origname = request.node.originalname
    node_id = str(request.node.name).replace(origname, "").replace("[", "").replace("]", "")
    command, mode, unit = request.param
    container = setup_container(unit)
    workflow_data = setup_workflow_data(request, tmpdir_factory, node_id)
    setup_metadata(request, tmpdir_factory, container, node_id)
    kwargs = {'target': 'all'}
    options = ["-d", str(workflow_data),
               "-j {}".format(request.config.option.ngs_threads),
               "--ri -k -p"]
    if unit == "conda":
        options += ["--use-conda"]
    if command == "list":
        results = "star_index"
        options += ["-l"]
    else:
        results = "steps (100%) done"

    Snakefile = workflow_data.join("Snakefile")
    return Snakefile, options, container, kwargs, results

import subprocess as sp
import pytest
import yaml
import gzip


@pytest.mark.skipif(not pytest.has_ngsfixtures,
                    reason=pytest.reason)
def test_empty_fastq_issue_37(tmpdir_factory, reference_data):
    from pytest_ngsfixtures.file import FixtureFile
    from pytest_ngsfixtures.os import safe_mktemp
    p = safe_mktemp(tmpdir_factory, "empty_fastq")
    FixtureFile(p.join("Snakefile"), src=pytest.snakefile, copy=True,
                setup=True)
    FixtureFile(p.join("config.yaml"), src=pytest.configfile,
                copy=True, setup=True)
    with p.join("config.fmt.yaml").open("w") as fh:
        fh.write(yaml.dump(
            {
                'settings': {'runfmt': '{SM}',
                             'samplefmt': '{SM}'},
                'samples': ["s1"],
            },
            default_flow_style=False))
    with p.join("sampleinfo.csv").open("w") as fh:
        fh.write("SM,fastq\ns1,s1_1.fastq.gz\n")
    with gzip.open(str(p.join("s1_1.fastq.gz")), "wt") as fh:
        fh.write("")
    Snakefile = p.join("Snakefile")
    options = ["-d", str(p), "--ri -k -p"]
    res = []
    from pytest_ngsfixtures.wm import snakemake
    for r in snakemake.run(Snakefile,
                           options=options,
                           iterable=True,
                           save=True,
                           stderr=sp.STDOUT,
                           target="s1.merge_rseqc/geneBody_coverage.geneBodyCoverage.txt"):
        print(r)
        res.append(r)
    results = "steps (100%) done"
    assert results in "".join(res)


@pytest.mark.skipif(not pytest.has_ngsfixtures,
                    reason=pytest.reason)
def test_make_gtf_28(tmpdir_factory, reference_data):
    from pytest_ngsfixtures.file import FixtureFile
    from pytest_ngsfixtures.os import safe_mktemp
    p = safe_mktemp(tmpdir_factory, "make_transcript_annot_gtf")
    target = p.join("../ref/ref-transcripts-tiny-pAcGFP1-N1-ERCC_spikes.gtf")
    if target.exists():
        target.remove()
    FixtureFile(p.join("Snakefile"), src=pytest.snakefile, copy=True,
                setup=True)
    FixtureFile(p.join("config.yaml"), src=pytest.configfile,
                copy=True, setup=True)
    p.join("config.fmt.yaml").open("w").write("samples:\n  - s1\n")
    with p.join("sampleinfo.csv").open("w") as fh:
        fh.write("SM,PU,fastq\ns1,foo,s1_1.fastq.gz\n")
    Snakefile = p.join("Snakefile")
    options = ["-d", str(p), "--ri -k -p"]
    res = []
    from pytest_ngsfixtures.wm import snakemake
    for r in snakemake.run(Snakefile,
                           options=options,
                           iterable=True,
                           save=True,
                           stderr=sp.STDOUT,
                           target="../ref/ref-transcripts-tiny-pAcGFP1-N1-ERCC_spikes.gtf"):
        print(r)
        res.append(r)
    results = "steps (100%) done"
    assert results in "".join(res)

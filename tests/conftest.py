#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pytest

try:
    from pytest_ngsfixtures.os import safe_mktemp
    from pytest_ngsfixtures import layout as sample_layout
    has_ngsfixtures = True
except:
    has_ngsfixtures = False
MISSING_PYTEST_NGSFIXTURES = "module pytest-ngsfixtures missing"
    

def pytest_namespace():
    return {'has_ngsfixtures': has_ngsfixtures,
            'reason': MISSING_PYTEST_NGSFIXTURES}


@pytest.mark.skipif(not has_ngsfixtures,
                    reason=MISSING_PYTEST_NGSFIXTURES)
@pytest.fixture(scope="session")
def reference_data(request, tmpdir_factory):
    path = safe_mktemp(tmpdir_factory, "ref")
    path = sample_layout.setup_reference_layout(
        path,
        label="ref",
        ignore_errors=True,
        scope="function")

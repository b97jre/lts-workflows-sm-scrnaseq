lts_workflows_sm_scrnaseq documentation
==========================================================

.. image:: https://readthedocs.org/projects/lts-workflows-sm-scrnaseq/badge/?version=latest
        :target: https://lts-workflows-sm-scrnaseq.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


single-cell RNA sequencing snakemake workflow

* Free software: GNU General Public License v3

Quickstart
----------

.. code-block:: console

   $ conda create -n py2.7 python=2.7 rpkmforgenes=1.0.1 rseqc=2.6.4
   $ conda create -n lts-workflows-sm-scrnaseq python=3.6
   $ source activate lts-workflows-sm-scrnaseq
   $ conda install -c scilifelab-lts lts-workflows-sm-scrnaseq
   $ lts_workflows_sm_scrnaseq
   $ lts_workflows_sm_scrnaseq -l
   $ lts_workflows_sm_scrnaseq all -d /path/to/workdir --configfile config.yaml
   $ lts_workflows_sm_scrnaseq --use-conda all -d /path/to/workdir --configfile config.yaml

.. code-block:: console

   $ docker pull scilifelablts/lts-workflows-sm-scrnaseq
   $ docker run scilifelablts/lts-workflows-sm-scrnaseq
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts-workflows-sm-scrnaseq -l
   $ docker run -v /path/to/workdir:/workspace -w /workspace scilifelablts/lts-workflows-sm-scrnaseq all
   

Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   usage
   configuration
   troubleshooting
   workflow
   contributing
   devel
   authors
   history
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

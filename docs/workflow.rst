.. _workflow:

Single-cell RNA sequencing workflow
========================================

This workflow does the following:

1. align reads with STAR to a genome or transcriptome reference
2. gene/transcript quantification with RSEM and/or rpkmforgenes
3. basic qc with RSeQC and MultiQC
   
Workflow
-----------

The figure below illustrates the workflow included in the test
directory. Here, use has been made of the additional rule mentioned in
:ref:`additional_advice` to generate the transcript annotation gtf.

.. image:: img/scrnaseq.png



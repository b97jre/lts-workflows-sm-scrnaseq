=======
History
=======

0.2.0 (2017-01-30)
------------------

* Simplify test setup
* Make pytest-ngsfixtures optional
* Use picard instead of samtools rules for sorting
  
* Update rseqc rule for empty fastq input (#37)
* Add multiqc rule (#33)
* Add rule for rpkm/count matrix (#31)
* Add gene entry to gtf file (#28)

0.1.0 (2017-02-12)
------------------

* First release on conda.

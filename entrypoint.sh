#! /bin/bash

set -e

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback
USER_ID=${LOCAL_USER_ID:-9001}
echo "Adding user $USER_ID"
useradd --shell /bin/bash -u $USER_ID -o -c "" -m -g 0 user
echo "Starting with UID : $USER_ID"
export HOME=/home/user

if [ "$1" = 'snakemake' ]; then
    shift
    . /opt/miniconda3/bin/activate snakemake && exec gosu user snakemake "$@"
elif [ "$1" = 'lts_workflows_sm_scrnaseq' ]; then
    shift
    . /opt/miniconda3/bin/activate snakemake && exec gosu user lts_workflows_sm_scrnaseq "$@"
elif [ "$1" = "/bin/bash" ]; then
    . /opt/miniconda3/bin/activate snakemake && exec gosu user "$@"
else
    # Always run wrapper script
    . /opt/miniconda3/bin/activate snakemake && exec gosu user lts_workflows_sm_scrnaseq "$@"
fi
